<?php

use App\Models\UserModel;
use Config\Services;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;


function getJWTFromRequest($authenticationHeader)
{
    if (is_null($authenticationHeader)) //jwt not parsing from header
    {
        throw new Exception('Token tidak ada atau tidak valid!');
    }
    //JWT is sent from client in the format Bearer XXXXXXXXX
    return explode(' ', $authenticationHeader)[1];
}

function validJWTFromRequest(string $encodeToken)
{
    $key = Services::getSecretKey();
    $decodeToken = JWT::decode($encodeToken, new Key($key, 'HS256'));
    $userModel = new UserModel();
    $userModel->findUserByEmailAddress($decodeToken->email);
}

function getSignedJWTFromRequest(string $email)
{
    $issuedAtTime = time();
    $tokenTimeToLive = getenv('JWT_TIME_TO_LIVE');
    $tokenExpiration = $issuedAtTime + $tokenTimeToLive;
    $payload = [
        'email' => $email,
        'iat' => $issuedAtTime,
        'exp' => $tokenExpiration,
    ];
    $jwt = JWT::encode($payload, Services::getSecretKey(), 'HS256');
    return $jwt;
}
