<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;
use ReflectionException;

class ApiAuth extends BaseController
{
    private function getJWTForUser(string $emailAddress, int $responseCode = ResponseInterface::HTTP_OK)
    {
        try {
            $model = new UserModel();
            $user = $model->findUserByEmailAddress($emailAddress);
            unset($user['password']);

            helper('jwt');
            return $this->getResponse([
                'messages' => 'Authentifikasi user berhasil',
                'user' => $user,
                'access_token' => getSignedJWTFromRequest($emailAddress)
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'error' => $e->getMessage()
            ], $responseCode);
        }
    }

    public function register()
    {

        $rules = [
            'email' => 'required|valid_email|is_unique[users.email]',
            'name' => 'required',
            'password' => 'required|min_length[3]'
        ];

        $messages = [
            "email" => [
                "required" => "Email tidak boleh kosong",
                "valid_email" => "Email tidak valid",
                "is_unique" => "Email sudah terdaftar",
            ],
            "name" => [
                "required" => "Nama tidak boleh kosong",
            ],
            "password" => [
                "required" => "password tidak boleh kosong",
                "min_length" => "password minimal 3 karakter"
            ],
        ];

        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $rules, $messages)) {
            return $this->getResponse(
                $this->validator->getErrors(),
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        $userModel = new UserModel();
        $userModel->save($input);

        return $this->getJWTForUser($input['email'], ResponseInterface::HTTP_CREATED);
    }

    public function login()
    {
        $rules = [
            'email' => 'required|valid_email|validateEmail[email]',
            'password' => 'required|min_length[3]|validateUser[email, password]'
        ];
        $messages = [
            "email" => [
                "required" => "Email tidak boleh kosong",
                "valid_email" => "Email tidak valid",
                "validateEmail" => "Email belum terdaptar"
            ],
            "password" => [
                "required" => "password tidak boleh kosong",
                'validateUser' => 'Password yang anda masukan salah',
                "min_length" => "password minimal 3 karakter"
            ],
        ];

        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $rules, $messages)) {
            return $this->getResponse(
                $this->validator->getErrors(),
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getJWTForUser($input['email']);
    }

    //batas
}
