<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        return view('welcome_message');
    }

    /**
     * untuk view client side dengan react
     */
    public function Ssr()
    {
        return view('client_side');
    }
}
