<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;
use Firebase\JWT\JWT;

class ApiRegister extends ResourceController
{
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */

    use ResponseTrait;

    //versi satu

    public function register()
    {
        helper(['form']);

        $rules = [
            'email' => 'required|valid_email|is_unique[users.email]',
            'name' => 'required',
            'password' => 'required|min_length[3]'
        ];
        $messages = [
            "email" => [
                "required" => "Email tidak boleh kosong",
                "valid_email" => "Email tidak valid",
                "is_unique" => "Email sudah terdaftar",
            ],
            "name" => [
                "required" => "Nama tidak boleh kosong",
            ],
            "password" => [
                "required" => "password tidak boleh kosong",
                "min_length" => "password minimal 3 karakter"
            ],
        ];
        if (!$this->validate($rules, $messages)) {
            $response = [
                'status' => 500,
                'error' => true,
                'messages' => $this->validator->getErrors()
            ];
        } else {
            $user = new UserModel();
            $data = [
                "name" => $this->request->getVar("name"),
                "email" => $this->request->getVar("email"),
                "password" => password_hash($this->request->getVar("password"), PASSWORD_BCRYPT)
            ];

            $save = $user->save($data);
            $findUser = $user->where("email", $this->request->getVar("email"))->first();
            if ($save) {
                $key = getenv('TOKEN_SECRET');
                $payload = [
                    "iat" => 1356999524,
                    "nbf" => 1357000000,
                    "uid" => $findUser['id'],
                    "email" => $findUser['email']
                ];

                $token = JWT::encode($payload, $key, 'HS256');
                $response = [
                    'status' => 200,
                    'error' => false,
                    'messages' => 'Registrasi user baru berhasil !',
                    'data' => [
                        'token' => $token,
                        'user' => $findUser
                    ]
                ];
            } else {
                $response = [
                    'status' => 500,
                    'error' => true,
                    'messages' => 'failed to register user'
                ];
            }
        }
        return $this->respondCreated($response);
    }

    public function login()
    {
        helper(['form']);

        $rules = [
            'email' => 'required|valid_email',
            'password' => 'required|min_length[3]'
        ];
        $messages = [
            "email" => [
                "required" => "Email tidak boleh kosong",
                "valid_email" => "Email tidak valid",
            ],
            "password" => [
                "required" => "password tidak boleh kosong",
                "min_length" => "password minimal 3 karakter"
            ],
        ];
        if (!$this->validate($rules, $messages)) {
            $response = [
                'status' => 500,
                'error' => true,
                'messages' => $this->validator->getErrors()
            ];
            return $this->respondCreated($response);
        } else {
            $user = new UserModel();

            $userData = $user->where("email", $this->request->getVar("email"))->first();
            if (!empty($userData)) {
                if (password_verify($this->request->getVar("password"), $userData['password'])) {
                    $key = getenv('TOKEN_SECRET');
                    $payload = [
                        "iat" => 1356999524,
                        "nbf" => 1357000000,
                        "uid" => $userData['id'],
                        "email" => $userData['email']
                    ];
                    $token = JWT::encode($payload, $key, 'HS256');
                    $response = [
                        'status' => 200,
                        'error' => false,
                        'messages' => 'login berhasil',
                        'data' => [
                            'access_token' => $token,
                            'user' => $userData
                        ]
                    ];
                    return $this->respondCreated($response);
                } else {
                    $response = [
                        'status' => 500,
                        'error' => true,
                        'messages' => 'password yang anda masukan salah',
                        'data' => []
                    ];
                    return $this->respondCreated($response);
                }
            } else {
                $response = [
                    'status' => 500,
                    'error' => true,
                    'messages' => 'Email tidak ditemukan',
                    'data' => []
                ];
                return $this->respondCreated($response);
            }
        }
    }


    public function index()
    {
        //
    }



    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        //
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        //
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        //
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        //
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        //
    }
}
