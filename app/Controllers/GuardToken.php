<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class GuardToken extends BaseController
{
    use ResponseTrait;

    public function index()
    {
        $key = getenv('TOKEN_SECRET');
        $headers = $this->request->getServer("HTTP_AUTHORIZATION");
        if (!$headers) return $this->failUnauthorized("Token required");
        $token = explode(' ', $headers)[1];

        try {
            $decode = JWT::decode($token, new key($key, 'HS256'));
            $response = [
                'status' => 200,
                'errors' => false,
                'messages' => 'User details',
                'data' => [
                    'profile' => $decode
                ]
            ];
            return $this->respondCreated($response);
        } catch (\Throwable $th) {
            $response = [
                'status' => 401,
                'error' => true,
                'messages' => 'Access denied',
                'error' => $th->getMessage()
            ];
            return $this->respondCreated($response);
        }
    }
}
