<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;
use App\Models\ClientModel;

class Client extends BaseController
{
    public function index()
    {
        $model = new ClientModel();
        return $this->getResponse([
            'messages' => 'Data client ditampilkan',
            'data' => $model->findAll()
        ]);
    }

    public function store()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|min_length[6]|max_length[50]|valid_email|is_unique[client.email]',
            'retainer_fee' => 'required|max_length[255]'
        ];
        $messages = [
            "email" => [
                "required" => "Email tidak boleh kosong",
                "valid_email" => "Email tidak valid",
                "is_unique" => "Email sudah terdaftar",
            ],
            "name" => [
                "required" => "Nama tidak boleh kosong",
            ],
            "retainer_fee" => [
                "required" => "biaya tidak boleh kosong",
                "max_length" => "data terlalu panjang"
            ],
        ];

        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $rules, $messages)) {
            return $this->getResponse(
                $this->validator->getErrors(),
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        $clientEmail = $input['email'];

        $model = new ClientModel();
        $model->save($input);

        //tampilkan data setelah berhasil input
        $client = $model->where('email', $clientEmail)->first();

        return $this->getResponse([
            'messages' => 'Data client berhasil ditambahkan',
            'data' => $client
        ]);
    }

    public function show($id)
    {
        try {

            $model = new ClientModel();
            $client = $model->findClinetById($id);

            return $this->getResponse([
                'messages' => 'Data client ditemukan',
                'data' => $client
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => 'Client dengan id tersebut tidak ditemukan'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    public function update($id)
    {
        try {

            $model = new ClientModel();
            $model->findClinetById($id);

            $input = $this->getRequestInput($this->request);

            $model->update($id, $input);

            $client = $model->findClinetById($id);

            return $this->getResponse([
                'messages' => 'Data client berhasil diupdate',
                'data' => $client
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    public function destroy($id)
    {
        try {

            $model = new ClientModel();

            $client = $model->findClinetById($id);

            $model->delete($client);


            return $this->getResponse([
                'messages' => 'Data client berhasil dihapus'
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    //batas
}
