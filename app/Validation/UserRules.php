<?php

namespace App\Validation;

use App\Models\UserModel;
use Exception;

class UserRules
{
    public function validateUser(string $str, string $fields, array $data)
    {
        try {
            $model = new UserModel();
            $user = $model->findUserByEmailAddress($data['email']);
            return password_verify($data['password'], $user['password']);
        } catch (Exception $e) {
            return false;
        }
    }

    public function validateEmail(string $str, string $fields, array $data)
    {
        try {
            $model = new UserModel();
            $user = $model->findUserByEmailAddress($data['email']);
            if ($user) return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
