<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => true
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 100
            ],
            'email_verified_at' => [
                'type' => 'timestamp',
                'null' => true
            ],
            'password' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'profile_photo_path' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'roles' => [
                'type' => 'CHAR',
                'constraint' => 1
            ],
            'created_at' => [
                'type' => 'timestamp',
                'null' => true
            ]
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('users');
    }

    public function down()
    {
        //
    }
}
