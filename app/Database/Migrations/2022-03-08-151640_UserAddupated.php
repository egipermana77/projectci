<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserAddupated extends Migration
{
    public function up()
    {
        $fields = ([
            'updated_at' => [
                'type' => 'timestamp',
                'after' => 'created_at'
            ],
            'deleted_at' => [
                'type' => 'timestamp',
                'after' => 'created_at'
            ],
        ]);
        $this->forge->addColumn('users', $fields);
    }

    public function down()
    {
        //
    }
}
